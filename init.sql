--DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
  id serial NOT NULL,
  name varchar(50),
  email varchar(50) NOT NULL,
  password varchar(1024) NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT users_unique_email UNIQUE (email)
);

--DROP TABLE IF EXISTS drugs;
CREATE TABLE IF NOT EXISTS drugs (
  id serial4 NOT NULL,
  name varchar(50) NOT NULL,
  approved boolean NOT NULL DEFAULT false,
  min_dose smallint NOT NULL,
  max_dose smallint NOT NULL,
  available_at timestamp NOT NULL,
  CONSTRAINT drugs_pkey PRIMARY KEY (id) 
);

--DROP TABLE IF EXISTS vaccinations;
CREATE TABLE IF NOT EXISTS vaccinations (
  id serial NOT NULL,
  name varchar(50) NOT NULL,
  drug_id integer NOT NULL,
  dose smallint NOT NULL,
  date timestamp NOT NULL,
  CONSTRAINT vaccinations_pkey PRIMARY KEY (id),
  constraint drug_id_fkey FOREIGN KEY (drug_id) REFERENCES public.drugs(id)
 );