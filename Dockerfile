FROM golang:1.19.2-alpine3.16 AS builder

RUN mkdir /app
ADD src /app
WORKDIR /app

RUN CGO_ENABLED=0 GOOS=linux go build -o main cmd/main.go

FROM alpine:3.16.2 AS production

COPY --from=builder /app/main /app/main

ARG SERVER_ADDRESS
ARG	SERVER_PORT
ARG	DB_USER
ARG	DB_PASSWD
ARG	DB_ADDR
ARG	DB_PORT
ARG	DB_NAME
EXPOSE ${SERVER_PORT}

RUN ls -l /app

CMD [ "/app/main" ]
