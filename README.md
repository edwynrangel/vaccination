## Run project

### Clone repo https
```
git clone https://gitlab.com/edwynrangel/vaccination.git
```

### Or clone with ssh
```
git clone git@gitlab.com:edwynrangel/vaccination.git
```
### Download dependencies
change to src directory and run the next command
```
cd src
go install github.com/golang/mock/mockgen@v1.6.0
go generate ./...
go mod tidy
```

### Run unit test
change to src directory and run the next command
```
cd src
PKG_LIST=$(go list ./... | grep -v /vendor/ | tr '\n' ' ')
go test -covermode=count -coverprofile coverage $PKG_LIST 
go tool cover -func=coverage
```

### Run app
from root directory, run the next command
```
docker-compose up -d
```

### Download image edwynrangel/vaccination
```
docker pull edwynrangel/vaccination:1.0.0
```

### Run app from code
change to src directory and run the next command
```
SERVER_ADDRESS=127.0.0.1 SERVER_PORT=8000 DB_USER=postgres DB_PASSWD=123 DB_ADDR=localhost DB_PORT=5432 DB_NAME="zeleri" go run cmd/main.go
```
**Note**: replace the environment values by the necessary values.