package vaccination

import (
	"time"

	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
)

type Vaccination struct {
	Id     int64     `db:"id"`
	Name   string    `db:"name"`
	DrugId int64     `db:"drug_id"`
	Dose   int16     `db:"dose"`
	Date   time.Time `db:"date"`
}

//go:generate mockgen -destination=../../mocks/domain/vaccination/mockVaccinationRepository.go -package=vaccination gitlab.com/edwynrangel/vaccination/domain/vaccination VaccinationRepository
type VaccinationRepository interface {
	Save(Vaccination) (*Vaccination, *errs.AppError)
	FindAll() ([]Vaccination, *errs.AppError)
	Update(Vaccination) (*Vaccination, *errs.AppError)
	Remove(id string) *errs.AppError
}

func (v Vaccination) ToNewVaccinationResponseDto() *dto.VaccinationResponse {

	return &dto.VaccinationResponse{
		Id:     v.Id,
		Name:   v.Name,
		DrugId: v.DrugId,
		Dose:   v.Dose,
		Date:   v.Date.Format("2006-01-02"),
	}
}

func (v Vaccination) CanNotApplyDose(min, max int16) bool {
	return v.Dose > max || v.Dose < min
}

func (v Vaccination) IsNotAvailableAt(availableAt time.Time) bool {
	return v.Date.Before(availableAt)
}
