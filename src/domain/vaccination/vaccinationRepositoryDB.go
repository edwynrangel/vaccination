package vaccination

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type VaccinationRepositoryDb struct {
	client *sqlx.DB
}

func (d VaccinationRepositoryDb) Save(v Vaccination) (*Vaccination, *errs.AppError) {
	query := "insert into vaccinations (name, drug_id, dose, date) values ($1, $2, $3, $4) returning id"
	row := d.client.QueryRow(query, v.Name, v.DrugId, v.Dose, v.Date)
	if row.Err() != nil {
		logger.Error(fmt.Sprintf("Error while creating new vaccination: %s", row.Err().Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}
	row.Scan(&v.Id)
	return &v, nil
}

func (d VaccinationRepositoryDb) FindAll() ([]Vaccination, *errs.AppError) {
	vaccinations := make([]Vaccination, 0)
	query := "select * from vaccinations order by id"
	err := d.client.Select(&vaccinations, query)
	if err != nil {
		logger.Error("Error while querying vaccinations table %s" + err.Error())
		return nil, errs.NewUnexpectedError("unexpected database error")
	}

	return vaccinations, nil
}

func (d VaccinationRepositoryDb) Update(v Vaccination) (*Vaccination, *errs.AppError) {
	query := `
		update vaccinations set 
			name = $1, 
			drug_id = $2, 
			dose = $3,  
			date = $4 
		where id = $5`

	result, err := d.client.Exec(query, v.Name, v.DrugId, v.Dose, v.Date, v.Id)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while updating new vaccination: %s", err.Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		logger.Error(fmt.Sprintf("Error while updating new vaccination: %s", err.Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}

	if rowsAffected == 0 {
		logger.Info("Vaccination not found")
		return nil, errs.NewNotFoundError("Vaccination not found")
	}

	return &v, nil
}

func (d VaccinationRepositoryDb) Remove(id string) *errs.AppError {
	query := "delete from vaccinations where id = $1"

	result, err := d.client.Exec(query, id)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while deleting new drug: %s", err.Error()))
		return errs.NewUnexpectedError("Unexpected error from database")
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		logger.Error(fmt.Sprintf("Error while deleting new drug: %s", err.Error()))
		return errs.NewUnexpectedError("Unexpected error from database")
	}

	if rowsAffected == 0 {
		logger.Info("No data found")
		return errs.NewNotFoundError("Vaccination not found")
	}

	return nil
}

func NewVaccinationRepositoryDb(dbClient *sqlx.DB) VaccinationRepositoryDb {
	return VaccinationRepositoryDb{dbClient}
}
