package user

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type UserRepositoryDb struct {
	client *sqlx.DB
}

func (d UserRepositoryDb) Save(u User) (*User, *errs.AppError) {
	user, appErr := d.FindByEmail(u.Email)
	if appErr != nil {
		return nil, appErr
	}

	if user.IfExistEmail(u.Email) {
		return nil, errs.NewValidationError("this email already exist")
	}

	password, err := u.HashPassword()
	if err != nil {
		logger.Error(fmt.Sprintf("Error while encripting password: %s", err.Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from api")
	}
	query := "insert into users (name, email, password) values ($1, $2, $3) returning id"
	row := d.client.QueryRow(query, u.Name, u.Email, password)
	if row.Err() != nil {
		logger.Error(fmt.Sprintf("Error while creating new user: %s", row.Err().Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}
	row.Scan(&u.Id)
	return &u, nil

}

func (d UserRepositoryDb) FindByEmail(email string) (*User, *errs.AppError) {
	var user User
	query := "select * from users where email = $1"
	err := d.client.Get(&user, query, email)
	if err != nil {
		if err != sql.ErrNoRows {
			logger.Error("Error while verifying login request from database: " + err.Error())
			return nil, errs.NewUnexpectedError("Unexpected database error")
		}
	}
	return &user, nil
}

func NewUserRepositoryDb(dbClient *sqlx.DB) UserRepositoryDb {
	return UserRepositoryDb{dbClient}
}
