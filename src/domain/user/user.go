package user

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/edwynrangel/vaccination/domain"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id       int64
	Name     string
	Email    string
	Password string
}

//go:generate mockgen -destination=../../mocks/domain/user/mockUserRepository.go -package=user gitlab.com/edwynrangel/vaccination/domain/user UserRepository
type UserRepository interface {
	Save(User) (*User, *errs.AppError)
	FindByEmail(email string) (*User, *errs.AppError)
}

func (u User) HashPassword() (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
	if err != nil {
		logger.Error("Error while encriptig password")
		return "", err
	}
	return string(bytes), nil
}

func (u User) ToNewUserResponseDto() *dto.UserResponse {

	return &dto.UserResponse{
		Id:    u.Id,
		Name:  u.Name,
		Email: u.Email,
	}
}

func (u User) InvalidPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	return err != nil
}

func (u User) ClaimsForAccessToken() domain.AccessTokenClaims {
	return domain.AccessTokenClaims{
		Name:  u.Name,
		Email: u.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(domain.ACCESS_TOKEN_DURATION).Unix(),
		},
	}
}

func (u User) IfExistEmail(email string) bool {
	return u.Email == email
}
