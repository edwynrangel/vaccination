package drug

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"

	_ "github.com/lib/pq"
)

type DrugRepositoryDb struct {
	client *sqlx.DB
}

func (d DrugRepositoryDb) Save(drug Drug) (*Drug, *errs.AppError) {
	query := "insert into drugs (name, approved, min_dose, max_dose, available_at) values ($1, $2, $3, $4, $5) returning id"
	row := d.client.QueryRow(query, drug.Name, drug.Approved, drug.MinDose, drug.MaxDose, drug.AvailableAt)
	if row.Err() != nil {
		logger.Error(fmt.Sprintf("Error while creating new drug: %s", row.Err().Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}
	row.Scan(&drug.Id)
	return &drug, nil

}

func (d DrugRepositoryDb) FindAll() ([]Drug, *errs.AppError) {
	drugs := make([]Drug, 0)
	query := "select id, name, approved, min_dose, max_dose, available_at from drugs order by id"
	err := d.client.Select(&drugs, query)
	if err != nil {
		logger.Error("Error while querying drugs table %s" + err.Error())
		return nil, errs.NewUnexpectedError("unexpected database error")
	}

	return drugs, nil
}

func (d DrugRepositoryDb) Update(drug Drug) (*Drug, *errs.AppError) {
	query := `
		update drugs set 
			name = $1, 
			approved = $2, 
			min_dose = $3, 
			max_dose = $4, 
			available_at = $5 
		where id = $6`

	result, err := d.client.Exec(query, drug.Name, drug.Approved, drug.MinDose, drug.MaxDose, drug.AvailableAt, drug.Id)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while updating new drug: %s", err.Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		logger.Error(fmt.Sprintf("Error while updating new drug: %s", err.Error()))
		return nil, errs.NewUnexpectedError("Unexpected error from database")
	}

	if rowsAffected == 0 {
		logger.Info("Drug not found")
		return nil, errs.NewNotFoundError("Drug not found")
	}

	return &drug, nil
}

func (d DrugRepositoryDb) Remove(id string) *errs.AppError {
	query := "delete from drugs where id = $1"

	result, err := d.client.Exec(query, id)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while deleting new drug: %s", err.Error()))
		return errs.NewUnexpectedError("Unexpected error from database")
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		logger.Error(fmt.Sprintf("Error while deleting new drug: %s", err.Error()))
		return errs.NewUnexpectedError("Unexpected error from database")
	}

	if rowsAffected == 0 {
		logger.Info("No data found")
		return errs.NewNotFoundError("Drug not found")
	}

	return nil
}

func (d DrugRepositoryDb) FindById(id int64) (*Drug, *errs.AppError) {
	var drug Drug
	query := "select * from drugs where id = $1"
	err := d.client.Get(&drug, query, id)
	if err != nil {
		if err != sql.ErrNoRows {
			logger.Error("Error while verifying login request from database: " + err.Error())
			return nil, errs.NewUnexpectedError("Unexpected database error")
		}
	}
	return &drug, nil
}

func NewDrugRepositoryDb(dbClient *sqlx.DB) DrugRepositoryDb {
	return DrugRepositoryDb{dbClient}
}
