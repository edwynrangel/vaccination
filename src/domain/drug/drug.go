package drug

import (
	"time"

	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
)

type Drug struct {
	Id          int64     `db:"id"`
	Name        string    `db:"name"`
	Approved    bool      `db:"approved"`
	MinDose     int16     `db:"min_dose"`
	MaxDose     int16     `db:"max_dose"`
	AvailableAt time.Time `db:"available_at"`
}

//go:generate mockgen -destination=../../mocks/domain/drug/mockDrugRepository.go -package=drug gitlab.com/edwynrangel/vaccination/domain/drug DrugRepository
type DrugRepository interface {
	Save(Drug) (*Drug, *errs.AppError)
	FindAll() ([]Drug, *errs.AppError)
	Update(Drug) (*Drug, *errs.AppError)
	Remove(id string) *errs.AppError
	FindById(id int64) (*Drug, *errs.AppError)
}

func (d Drug) ToNewDrugResponseDto() *dto.DrugResponse {

	return &dto.DrugResponse{
		Id:          d.Id,
		Name:        d.Name,
		Approved:    d.Approved,
		MinDose:     d.MinDose,
		MaxDose:     d.MaxDose,
		AvailableAt: d.AvailableAt.Format("2006-01-02"),
	}
}
