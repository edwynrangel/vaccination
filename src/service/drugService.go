package service

import (
	"fmt"
	"time"

	"gitlab.com/edwynrangel/vaccination/domain/drug"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

//go:generate mockgen -destination=../mocks/service/mockDrugService.go -package=service gitlab.com/edwynrangel/vaccination/service DrugService
type DrugService interface {
	NewDrug(dto.DrugRequest) (*dto.DrugResponse, *errs.AppError)
	GetAllDrug() ([]dto.DrugResponse, *errs.AppError)
	UpdateDrug(dto.DrugRequest) (*dto.DrugResponse, *errs.AppError)
	RemoveDrug(id string) *errs.AppError
}

type DefaultDrugService struct {
	repo drug.DrugRepository
}

func (s DefaultDrugService) NewDrug(request dto.DrugRequest) (*dto.DrugResponse, *errs.AppError) {
	err := request.Validate()
	if err != nil {
		return nil, err
	}
	availableAt, _ := time.Parse("2006-01-02", request.AvailableAt)
	drug := drug.Drug{
		Name:        request.Name,
		Approved:    request.Approved,
		MinDose:     request.MinDose,
		MaxDose:     request.MaxDose,
		AvailableAt: availableAt,
	}

	newDrug, AppErr := s.repo.Save(drug)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	response := newDrug.ToNewDrugResponseDto()

	return response, nil
}

func (s DefaultDrugService) GetAllDrug() ([]dto.DrugResponse, *errs.AppError) {
	drugs, err := s.repo.FindAll()
	if err != nil {
		logger.Error(fmt.Sprintf("Error: %s", err.Message))
		return nil, err
	}
	response := make([]dto.DrugResponse, 0)
	for _, drug := range drugs {
		response = append(response, *drug.ToNewDrugResponseDto())
	}
	return response, nil
}

func (s DefaultDrugService) UpdateDrug(request dto.DrugRequest) (*dto.DrugResponse, *errs.AppError) {

	err := request.Validate()
	if err != nil {
		return nil, err
	}
	availableAt, _ := time.Parse("2006-01-02", request.AvailableAt)
	drug := drug.Drug{
		Id:          request.Id,
		Name:        request.Name,
		Approved:    request.Approved,
		MinDose:     request.MinDose,
		MaxDose:     request.MaxDose,
		AvailableAt: availableAt,
	}

	updateDrug, AppErr := s.repo.Update(drug)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	response := updateDrug.ToNewDrugResponseDto()

	return response, nil
}

func (s DefaultDrugService) RemoveDrug(id string) *errs.AppError {
	err := s.repo.Remove(id)
	if err != nil {
		logger.Error(fmt.Sprintf("Error: %s", err.Message))
		return err
	}
	return nil
}

func NewDrugService(repository drug.DrugRepository) DefaultDrugService {
	return DefaultDrugService{repository}
}
