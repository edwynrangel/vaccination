package service

import (
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	realDrug "gitlab.com/edwynrangel/vaccination/domain/drug"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/mocks/domain/drug"
)

func Test_should_return_a_validation_error_response_when_the_request_is_not_validated_to_new_drug(t *testing.T) {
	// Arrange
	request := dto.DrugRequest{
		Name: "",
	}
	service := NewDrugService(nil)
	// Act
	_, appError := service.NewDrug(request)
	// Assert
	if appError == nil {
		t.Error("failed while testing the new drug validation")
	}
}

var mockDrugRepo *drug.MockDrugRepository
var drugService DrugService

func drugSetup(t *testing.T) func() {
	ctrl := gomock.NewController(t)
	mockDrugRepo = drug.NewMockDrugRepository(ctrl)
	drugService = NewDrugService(mockDrugRepo)
	return func() {
		drugService = nil
		defer ctrl.Finish()
	}
}

func Test_should_return_an_error_from_the_server_side_if_the_new_drug_cannot_be_created(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	req := dto.DrugRequest{
		Name:        "test 1",
		Approved:    true,
		MinDose:     1,
		MaxDose:     2,
		AvailableAt: "2022-10-28",
	}

	availableAt, _ := time.Parse("2006-01-02", req.AvailableAt)
	d := realDrug.Drug{
		Name:        req.Name,
		Approved:    req.Approved,
		MinDose:     req.MinDose,
		MaxDose:     req.MaxDose,
		AvailableAt: availableAt,
	}
	mockDrugRepo.EXPECT().Save(d).Return(nil, errs.NewUnexpectedError("Unexpected database error"))
	// Act
	_, appError := drugService.NewDrug(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new drug")
	}

}

func Test_should_return_new_drug_response_when_a_new_drug_is_saved_successfully(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	req := dto.DrugRequest{
		Name:        "test 1",
		Approved:    true,
		MinDose:     1,
		MaxDose:     2,
		AvailableAt: "2022-10-28",
	}

	availableAt, _ := time.Parse("2006-01-02", req.AvailableAt)
	d := realDrug.Drug{
		Name:        req.Name,
		Approved:    req.Approved,
		MinDose:     req.MinDose,
		MaxDose:     req.MaxDose,
		AvailableAt: availableAt,
	}

	drugWithId := d
	drugWithId.Id = 1
	mockDrugRepo.EXPECT().Save(d).Return(&drugWithId, nil)
	// Act
	newDrug, appError := drugService.NewDrug(req)

	// Assert
	if appError != nil {
		t.Error("Test failed while creating new drug")
	}
	if newDrug.Id != drugWithId.Id {
		t.Error("Failed while mathching new drug id")
	}
}

func Test_should_return_list_drug_response_successfully(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	drugs := []realDrug.Drug{
		{Id: 1, Name: "test 1", Approved: true, MinDose: 1, MaxDose: 2, AvailableAt: time.Now()},
		{Id: 2, Name: "test 2", Approved: true, MinDose: 2, MaxDose: 3, AvailableAt: time.Now()},
	}

	mockDrugRepo.EXPECT().FindAll().Return(drugs, nil)
	// Act
	listDrugs, appError := drugService.GetAllDrug()

	// Assert
	if appError != nil {
		t.Error("Test failed while get drug list")
	}
	if len(listDrugs) == 0 {
		t.Error("Failed while get drug list")
	}
}

func Test_should_return_an_error_from_the_server_side(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	mockDrugRepo.EXPECT().FindAll().Return(nil, errs.NewUnexpectedError("Unexpected database error"))
	// Act
	_, appError := drugService.GetAllDrug()

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for list drug")
	}

}

func Test_should_return_a_validation_error_response_when_the_request_is_not_validated_to_update_drug(t *testing.T) {
	// Arrange
	request := dto.DrugRequest{
		Name: "",
	}
	service := NewDrugService(nil)
	// Act
	_, appError := service.UpdateDrug(request)
	// Assert
	if appError == nil {
		t.Error("failed while testing the new drug validation")
	}
}

func Test_should_return_an_error_from_the_server_side_if_the_drug_cannot_be_updated(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	req := dto.DrugRequest{
		Name:        "test 1",
		Approved:    true,
		MinDose:     1,
		MaxDose:     2,
		AvailableAt: "2022-10-28",
	}

	availableAt, _ := time.Parse("2006-01-02", req.AvailableAt)
	d := realDrug.Drug{
		Name:        "test 1",
		Approved:    true,
		MinDose:     1,
		MaxDose:     2,
		AvailableAt: availableAt,
	}
	mockDrugRepo.EXPECT().Update(d).Return(nil, errs.NewUnexpectedError("Unexpected database error"))
	// Act
	_, appError := drugService.UpdateDrug(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for drug")
	}

}

func Test_should_return_drug_response_when_a_drug_is_updated_successfully(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	req := dto.DrugRequest{
		Name:        "test 1",
		Approved:    true,
		MinDose:     1,
		MaxDose:     2,
		AvailableAt: time.Now().Format("2006-01-02"),
	}

	availableAt, _ := time.Parse("2006-01-02", req.AvailableAt)
	d := realDrug.Drug{
		Name:        req.Name,
		Approved:    req.Approved,
		MinDose:     req.MinDose,
		MaxDose:     req.MaxDose,
		AvailableAt: availableAt,
	}

	drugWithId := d
	drugWithId.Id = 2
	mockDrugRepo.EXPECT().Update(d).Return(&drugWithId, nil)
	// Act
	updateDrug, appError := drugService.UpdateDrug(req)

	// Assert
	if appError != nil {
		t.Error("Test failed while updating drug")
	}
	if updateDrug.Id != drugWithId.Id {
		t.Error("Failed while mathching drug id")
	}
}

func Test_should_return_an_error_from_the_server_side_if_the_drug_cannot_be_deleted(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	mockDrugRepo.EXPECT().Remove("2").Return(errs.NewUnexpectedError("Unexpected database error"))
	// Act
	appError := drugService.RemoveDrug("2")

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new drug")
	}

}

func Test_should_return_anything_response_when_a_drug_is_delete_successfully(t *testing.T) {
	// Arrange
	teardown := drugSetup(t)
	defer teardown()

	mockDrugRepo.EXPECT().Remove("2").Return(nil)
	// Act
	appError := drugService.RemoveDrug("2")

	// Assert
	if appError != nil {
		t.Error("Test failed while updating drug")
	}
}
