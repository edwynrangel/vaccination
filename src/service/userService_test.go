package service

import (
	"testing"

	"github.com/golang/mock/gomock"
	realUser "gitlab.com/edwynrangel/vaccination/domain/user"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/mocks/domain/user"
)

func Test_should_return_a_validation_error_response_when_the_request_is_not_validated_to_new_user(t *testing.T) {
	// Arrange
	request := dto.UserRequest{
		Email: "",
	}
	service := NewUserService(nil)
	// Act
	_, appError := service.NewUser(request)
	// Assert
	if appError == nil {
		t.Error("failed while testing the new user validation")
	}
}

var mockUserRepo *user.MockUserRepository
var userService UserService

func userSetup(t *testing.T) func() {
	ctrl := gomock.NewController(t)
	mockUserRepo = user.NewMockUserRepository(ctrl)
	userService = NewUserService(mockUserRepo)
	return func() {
		userService = nil
		defer ctrl.Finish()
	}
}

func Test_should_return_an_error_from_the_server_side_if_the_new_user_cannot_be_created(t *testing.T) {
	// Arrange
	teardown := userSetup(t)
	defer teardown()

	req := dto.UserRequest{
		Name:     "test 1",
		Email:    "edwyn@mail.com",
		Password: "123",
	}

	u := realUser.User{
		Name:     req.Name,
		Email:    req.Email,
		Password: req.Password,
	}
	mockUserRepo.EXPECT().Save(u).Return(nil, errs.NewUnexpectedError("Unexpected database error"))
	// Act
	_, appError := userService.NewUser(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new user")
	}

}

func Test_should_return_new_user_response_when_a_new_user_is_saved_successfully(t *testing.T) {
	// Arrange
	teardown := userSetup(t)
	defer teardown()

	req := dto.UserRequest{
		Name:     "test 1",
		Email:    "edwyn@mail.com",
		Password: "123",
	}

	u := realUser.User{
		Name:     req.Name,
		Email:    req.Email,
		Password: req.Password,
	}

	userWithId := u
	userWithId.Id = 1
	mockUserRepo.EXPECT().Save(u).Return(&userWithId, nil)
	// Act
	newUser, appError := userService.NewUser(req)

	// Assert
	if appError != nil {
		t.Error("Test failed while creating new user")
	}
	if newUser.Id != userWithId.Id {
		t.Error("Failed while mathching new user id")
	}
}
