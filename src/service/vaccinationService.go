package service

import (
	"fmt"
	"time"

	"gitlab.com/edwynrangel/vaccination/domain/drug"
	"gitlab.com/edwynrangel/vaccination/domain/vaccination"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

//go:generate mockgen -destination=../mocks/service/mockVaccinationService.go -package=service gitlab.com/edwynrangel/vaccination/service VaccinationService
type VaccinationService interface {
	NewVaccination(dto.VaccinationRequest) (*dto.VaccinationResponse, *errs.AppError)
	GetAllVaccination() ([]dto.VaccinationResponse, *errs.AppError)
	UpdateVaccination(dto.VaccinationRequest) (*dto.VaccinationResponse, *errs.AppError)
	RemoveVaccination(id string) *errs.AppError
}

type DefaultVaccinationService struct {
	repo     vaccination.VaccinationRepository
	repoDrug drug.DrugRepository
}

func (s DefaultVaccinationService) NewVaccination(request dto.VaccinationRequest) (*dto.VaccinationResponse, *errs.AppError) {
	err := request.Validate()
	if err != nil {
		return nil, err
	}

	drug, AppErr := s.repoDrug.FindById(request.DrugId)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	if drug.Id == 0 {
		return nil, errs.NewNotFoundError("drug_id not found")
	}

	date, _ := time.Parse("2006-01-02", request.Date)
	v := vaccination.Vaccination{
		Name:   request.Name,
		DrugId: request.DrugId,
		Dose:   request.Dose,
		Date:   date,
	}

	if v.CanNotApplyDose(drug.MinDose, drug.MaxDose) {
		return nil, errs.NewNotFoundError("can not apply dose")
	}

	if v.IsNotAvailableAt(drug.AvailableAt) {
		return nil, errs.NewNotFoundError("date is not available")
	}

	newVaccination, AppErr := s.repo.Save(v)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	response := newVaccination.ToNewVaccinationResponseDto()

	return response, nil
}

func (s DefaultVaccinationService) GetAllVaccination() ([]dto.VaccinationResponse, *errs.AppError) {
	vaccinations, err := s.repo.FindAll()
	if err != nil {
		logger.Error(fmt.Sprintf("Error: %s", err.Message))
		return nil, err
	}
	response := make([]dto.VaccinationResponse, 0)
	for _, v := range vaccinations {
		response = append(response, *v.ToNewVaccinationResponseDto())
	}
	return response, nil
}

func (s DefaultVaccinationService) UpdateVaccination(request dto.VaccinationRequest) (*dto.VaccinationResponse, *errs.AppError) {

	err := request.Validate()
	if err != nil {
		return nil, err
	}

	drug, AppErr := s.repoDrug.FindById(request.DrugId)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	if drug.Id == 0 {
		return nil, errs.NewNotFoundError("drug_id not found")
	}

	date, _ := time.Parse("2006-01-02", request.Date)
	v := vaccination.Vaccination{
		Id:     request.Id,
		Name:   request.Name,
		DrugId: request.DrugId,
		Dose:   request.Dose,
		Date:   date,
	}

	if v.CanNotApplyDose(drug.MinDose, drug.MaxDose) {
		return nil, errs.NewNotFoundError("can not apply dose")
	}

	if v.IsNotAvailableAt(drug.AvailableAt) {
		return nil, errs.NewNotFoundError("date is not available")
	}

	updateVaccination, AppErr := s.repo.Update(v)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	response := updateVaccination.ToNewVaccinationResponseDto()

	return response, nil
}

func (s DefaultVaccinationService) RemoveVaccination(id string) *errs.AppError {
	err := s.repo.Remove(id)
	if err != nil {
		logger.Error(fmt.Sprintf("Error: %s", err.Message))
		return err
	}
	return nil
}

func NewVaccinationService(repository vaccination.VaccinationRepository, repositoryDrug drug.DrugRepository) DefaultVaccinationService {
	return DefaultVaccinationService{repo: repository, repoDrug: repositoryDrug}
}
