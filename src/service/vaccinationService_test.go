package service

import (
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	realDrug "gitlab.com/edwynrangel/vaccination/domain/drug"
	realVacc "gitlab.com/edwynrangel/vaccination/domain/vaccination"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/mocks/domain/drug"
	"gitlab.com/edwynrangel/vaccination/mocks/domain/vaccination"
)

func Test_should_return_a_validation_error_response_when_the_request_is_not_validated_to_new_vaccination(t *testing.T) {
	// Arrange
	request := dto.VaccinationRequest{
		Name: "",
	}
	service := NewVaccinationService(nil, nil)
	// Act
	_, appError := service.NewVaccination(request)
	// Assert
	if appError == nil {
		t.Error("failed while testing the new vaccination validation")
	}
}

var mockVaccRepo *vaccination.MockVaccinationRepository
var vaccinationService VaccinationService

func setup(t *testing.T) func() {
	ctrl := gomock.NewController(t)
	mockVaccRepo = vaccination.NewMockVaccinationRepository(ctrl)
	mockDrugRepo = drug.NewMockDrugRepository(ctrl)
	vaccinationService = NewVaccinationService(mockVaccRepo, mockDrugRepo)
	return func() {
		vaccinationService = nil
		defer ctrl.Finish()
	}
}

func Test_should_return_an_error_from_the_server_side_if_the_new_vaccination_cannot_be_created_bad_drug_id(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	req := dto.VaccinationRequest{
		Name:   "test 1",
		DrugId: 2,
		Dose:   1,
		Date:   "2022-10-28",
	}

	mockDrugRepo.EXPECT().FindById(int64(2)).Return(nil, errs.NewUnexpectedError("Unexpected database error"))

	// Act
	_, appError := vaccinationService.NewVaccination(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new vaccination")
	}

}

func Test_should_return_an_error_from_the_server_side_if_the_new_vaccination_cannot_be_created_by_not_found_drug(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	req := dto.VaccinationRequest{
		Name:   "test 1",
		DrugId: 2,
		Dose:   1,
		Date:   "2022-10-28",
	}

	mockDrugRepo.EXPECT().FindById(int64(2)).Return(nil, errs.NewNotFoundError("drug_id not found"))

	// Act
	_, appError := vaccinationService.NewVaccination(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new vaccination")
	}

}

func Test_should_return_an_error_from_the_server_side_if_the_new_vaccination_cannot_be_created_bad_dose(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	req := dto.VaccinationRequest{
		Name:   "test 1",
		DrugId: 2,
		Dose:   4,
		Date:   "2022-10-28",
	}

	mockDrugRepo.EXPECT().FindById(int64(2)).Return(&realDrug.Drug{Id: 2, Name: "test 1", Approved: true, MinDose: 1, MaxDose: 3, AvailableAt: time.Now()}, nil)

	// Act
	_, appError := vaccinationService.NewVaccination(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new vaccination")
	}

}

func Test_should_return_an_error_from_the_server_side_if_the_new_vaccination_cannot_be_created_bad_date(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	req := dto.VaccinationRequest{
		Name:   "test 1",
		DrugId: 2,
		Dose:   1,
		Date:   "2022-10-28",
	}

	mockDrugRepo.EXPECT().FindById(int64(2)).Return(&realDrug.Drug{Id: 2, Name: "test 1", Approved: true, MinDose: 1, MaxDose: 3, AvailableAt: time.Now()}, nil)

	// Act
	_, appError := vaccinationService.NewVaccination(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new vaccination")
	}

}

func Test_should_return_list_vaccination_response_successfully(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	vaccination := []realVacc.Vaccination{
		{Id: 1, Name: "test 1", DrugId: 2, Dose: 1, Date: time.Now()},
		{Id: 2, Name: "test 2", DrugId: 2, Dose: 2, Date: time.Now()},
	}

	mockVaccRepo.EXPECT().FindAll().Return(vaccination, nil)
	// Act
	listVacc, appError := vaccinationService.GetAllVaccination()

	// Assert
	if appError != nil {
		t.Error("Test failed while get vaccination list")
	}
	if len(listVacc) == 0 {
		t.Error("Failed while get vaccination list")
	}
}

func Test_should_return_an_error_from_the_server_side_list_vaccination(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	mockVaccRepo.EXPECT().FindAll().Return(nil, errs.NewUnexpectedError("Unexpected database error"))
	// Act
	_, appError := vaccinationService.GetAllVaccination()

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for list drug")
	}

}

func Test_should_return_an_error_from_the_server_side_if_the_vaccination_cannot_be_deleted(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	mockVaccRepo.EXPECT().Remove("2").Return(errs.NewUnexpectedError("Unexpected database error"))
	// Act
	appError := vaccinationService.RemoveVaccination("2")

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new vaccination")
	}

}

func Test_should_return_anything_response_when_a_vaccination_is_delete_successfully(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	mockVaccRepo.EXPECT().Remove("2").Return(nil)
	// Act
	appError := vaccinationService.RemoveVaccination("2")

	// Assert
	if appError != nil {
		t.Error("Test failed while updating vaccination")
	}
}

func Test_should_return_a_validation_error_response_when_the_request_is_not_validated_to_updated_vaccination(t *testing.T) {
	// Arrange
	request := dto.VaccinationRequest{
		Name: "",
	}
	service := NewVaccinationService(nil, nil)
	// Act
	_, appError := service.UpdateVaccination(request)
	// Assert
	if appError == nil {
		t.Error("failed while testing the new vaccination validation")
	}
}

func Test_should_return_an_error_from_the_server_side_if_vaccination_cannot_be_update_bad_drug_id(t *testing.T) {
	// Arrange
	teardown := setup(t)
	defer teardown()

	req := dto.VaccinationRequest{
		Name:   "test 1",
		DrugId: 2,
		Dose:   1,
		Date:   "2022-10-28",
	}

	mockDrugRepo.EXPECT().FindById(int64(2)).Return(nil, errs.NewUnexpectedError("Unexpected database error"))

	// Act
	_, appError := vaccinationService.UpdateVaccination(req)

	// Assert
	if appError == nil {
		t.Error("Test failed while validating error for new vaccination")
	}

}
