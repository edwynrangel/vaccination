package service

import (
	"fmt"

	"gitlab.com/edwynrangel/vaccination/domain/user"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

//go:generate mockgen -destination=../mocks/service/mockUserService.go -package=service gitlab.com/edwynrangel/vaccination/service UserService
type UserService interface {
	NewUser(dto.UserRequest) (*dto.UserResponse, *errs.AppError)
}

type DefaultUserService struct {
	repo user.UserRepository
}

func (s DefaultUserService) NewUser(request dto.UserRequest) (*dto.UserResponse, *errs.AppError) {
	err := request.Validate()
	if err != nil {
		return nil, err
	}
	User := user.User{
		Name:     request.Name,
		Email:    request.Email,
		Password: request.Password,
	}

	newUser, AppErr := s.repo.Save(User)
	if AppErr != nil {
		logger.Error(fmt.Sprintf("Error: %s", AppErr.Message))
		return nil, AppErr
	}

	response := newUser.ToNewUserResponseDto()

	return response, nil
}

func NewUserService(repository user.UserRepository) DefaultUserService {
	return DefaultUserService{repository}
}
