package service

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/edwynrangel/vaccination/domain"
	"gitlab.com/edwynrangel/vaccination/domain/user"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type AuthService interface {
	Login(dto.LoginRequest) (*dto.LoginResponse, *errs.AppError)
	Verify(string) *errs.AppError
}

type DefaultAuthService struct {
	repo user.UserRepository
}

func (s DefaultAuthService) Login(request dto.LoginRequest) (*dto.LoginResponse, *errs.AppError) {
	err := request.Validate()
	if err != nil {
		return nil, err
	}

	u, appErr := s.repo.FindByEmail(request.Email)
	if appErr != nil {
		return nil, appErr
	}

	if u == nil {
		return nil, errs.NewAuthenticationError("invalid credentials")
	}

	if u.InvalidPassword(request.Password) {
		return nil, errs.NewAuthenticationError("invalid credentials")
	}

	claims := u.ClaimsForAccessToken()
	authToken := domain.NewAuthToken(claims)
	accessToken, appErr := authToken.NewAccessToken()
	if appErr != nil {
		return nil, appErr
	}

	return &dto.LoginResponse{Token: accessToken}, nil
}

func (s DefaultAuthService) Verify(token string) *errs.AppError {
	jwtToken, err := jwtTokenFromString(token)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while parsing token: %s", err.Error()))
		return errs.NewAuthorizationError(err.Error())
	}
	if !jwtToken.Valid {
		return errs.NewAuthorizationError("Invalid token")
	}
	return nil
}

func jwtTokenFromString(tokenString string) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(tokenString, &domain.AccessTokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(domain.HMAC_SAMPLE_SECRET), nil
	})
	if err != nil {
		logger.Error("Error while parsing token: " + err.Error())
		return nil, err
	}
	return token, nil
}

func NewAuthService(repository user.UserRepository) DefaultAuthService {
	return DefaultAuthService{repository}
}
