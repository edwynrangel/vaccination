package dto

import (
	"fmt"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type VaccinationRequest struct {
	Id     int64  `json:"id,omitempty"`
	Name   string `json:"name" valid:"required~name is required"`
	DrugId int64  `json:"drug_id" valid:"required~drug_id is required"`
	Dose   int16  `json:"dose" valid:"required~dose is required"`
	Date   string `json:"date" valid:"required~date is required"`
}

type VaccinationResponse struct {
	Id     int64  `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	DrugId int64  `json:"drug_id,omitempty"`
	Dose   int16  `json:"dose,omitempty"`
	Date   string `json:"date,omitempty"`
}

func (dr VaccinationRequest) Validate() *errs.AppError {
	_, err := valid.ValidateStruct(dr)
	if err != nil {
		logger.Error(fmt.Sprintf("Error validate struct %+v\n", err))
		return errs.NewValidationError(err.Error())
	}

	return nil
}
