package dto

import (
	"fmt"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type LoginRequest struct {
	Email    string `json:"email" valid:"required~email is required"`
	Password string `json:"password" valid:"required~password is required"`
}

type LoginResponse struct {
	Token string `json:"token,omitempty"`
}

func (lr LoginRequest) Validate() *errs.AppError {
	_, err := valid.ValidateStruct(lr)
	if err != nil {
		logger.Error(fmt.Sprintf("Error validate struct %+v\n", err))
		return errs.NewValidationError(err.Error())
	}

	return nil
}
