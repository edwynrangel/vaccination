package dto

import (
	"fmt"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type DrugRequest struct {
	Id          int64  `json:"id,omitempty"`
	Name        string `json:"name" valid:"required~name is required"`
	Approved    bool   `json:"approved"`
	MinDose     int16  `json:"min_dose" valid:"required~min_dose is required"`
	MaxDose     int16  `json:"max_dose" valid:"required~max_dose is required"`
	AvailableAt string `json:"available_at" valid:"required~available_at is required"`
}

type DrugResponse struct {
	Id          int64  `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Approved    bool   `json:"approved"`
	MinDose     int16  `json:"min_dose,omitempty"`
	MaxDose     int16  `json:"max_dose,omitempty"`
	AvailableAt string `json:"available_at,omitempty"`
}

func (dr DrugRequest) Validate() *errs.AppError {
	_, err := valid.ValidateStruct(dr)
	if err != nil {
		logger.Error(fmt.Sprintf("Error validate struct %+v\n", err))
		return errs.NewValidationError(err.Error())
	}

	return nil
}
