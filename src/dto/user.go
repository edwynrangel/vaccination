package dto

import (
	"fmt"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/logger"
)

type UserRequest struct {
	Name     string `json:"name"`
	Email    string `json:"email" valid:"required~email is required"`
	Password string `json:"password" valid:"required~password is required"`
}

type UserResponse struct {
	Id    int64  `json:"id,omitempty"`
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

func (ur UserRequest) Validate() *errs.AppError {
	_, err := valid.ValidateStruct(ur)
	if err != nil {
		logger.Error(fmt.Sprintf("Error validate struct %+v\n", err))
		return errs.NewValidationError(err.Error())
	}

	return nil
}
