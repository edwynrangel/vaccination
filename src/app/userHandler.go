package app

import (
	"encoding/json"
	"net/http"

	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/logger"
	"gitlab.com/edwynrangel/vaccination/service"
)

type UserHandler struct {
	service service.UserService
}

func (dh UserHandler) newUser(w http.ResponseWriter, r *http.Request) {
	var request dto.UserRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Error("Error decode json")
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		User, appErr := dh.service.NewUser(request)
		if appErr != nil {
			writeResponse(w, appErr.Code, appErr.AsMessage())
		} else {
			writeResponse(w, http.StatusCreated, User)
		}
	}
}
