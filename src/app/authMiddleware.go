package app

import (
	"net/http"
	"strings"

	"gitlab.com/edwynrangel/vaccination/errs"
	"gitlab.com/edwynrangel/vaccination/service"
)

type AuthMiddleware struct {
	service service.AuthService
}

func (a AuthMiddleware) authorizationHandler() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			authHeader := r.Header.Get("Authorization")

			if authHeader != "" {
				token := getTokenFromHeader(authHeader)

				if err := a.service.Verify(token); err != nil {
					writeResponse(w, err.Code, err.AsMessage())
				} else {
					next.ServeHTTP(w, r)
				}
			} else {
				appErr := errs.AppError{Code: http.StatusUnauthorized, Message: "missing token"}
				writeResponse(w, appErr.Code, appErr.AsMessage())
			}
		})
	}
}

func getTokenFromHeader(header string) string {
	splitToken := strings.Split(header, "Bearer")
	if len(splitToken) == 2 {
		return strings.TrimSpace(splitToken[1])
	}
	return ""
}
