package app

import (
	"encoding/json"
	"net/http"

	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/logger"
	"gitlab.com/edwynrangel/vaccination/service"
)

type AuthHandler struct {
	service service.AuthService
}

func (ah AuthHandler) login(w http.ResponseWriter, r *http.Request) {
	var request dto.LoginRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Error("Error decode json")
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		Auth, appErr := ah.service.Login(request)
		if appErr != nil {
			writeResponse(w, appErr.Code, appErr.AsMessage())
		} else {
			writeResponse(w, http.StatusCreated, Auth)
		}
	}
}
