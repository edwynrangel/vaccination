package app

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/logger"
	"gitlab.com/edwynrangel/vaccination/service"
)

type VaccinationHandler struct {
	service service.VaccinationService
}

func (vh VaccinationHandler) newVaccination(w http.ResponseWriter, r *http.Request) {
	var request dto.VaccinationRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Error("Error decode json")
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		drug, appErr := vh.service.NewVaccination(request)
		if appErr != nil {
			writeResponse(w, appErr.Code, appErr.AsMessage())
		} else {
			writeResponse(w, http.StatusCreated, drug)
		}
	}
}

func (vh VaccinationHandler) getAllVaccination(writer http.ResponseWriter, request *http.Request) {

	drugs, err := vh.service.GetAllVaccination()
	if err != nil {
		writeResponse(writer, err.Code, err.AsMessage())
	} else {
		writeResponse(writer, http.StatusOK, drugs)
	}

}

func (vh *VaccinationHandler) updateVaccination(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	drugId := vars["id"]
	var request dto.VaccinationRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Error("Error decode json")
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		request.Id, err = strconv.ParseInt(drugId, 10, 64)
		if err != nil {
			logger.Error("Error parse int")
			writeResponse(w, http.StatusInternalServerError, err.Error())
		}
		drugs, err := vh.service.UpdateVaccination(request)
		if err != nil {
			writeResponse(w, err.Code, err.AsMessage())
		} else {
			writeResponse(w, http.StatusOK, drugs)
		}
	}

}

func (vh *VaccinationHandler) removeVaccination(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	drugId := vars["id"]

	err := vh.service.RemoveVaccination(drugId)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusNoContent, dto.VaccinationResponse{})
	}

}
