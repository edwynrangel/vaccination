package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"gitlab.com/edwynrangel/vaccination/domain/drug"
	"gitlab.com/edwynrangel/vaccination/domain/user"
	"gitlab.com/edwynrangel/vaccination/domain/vaccination"
	"gitlab.com/edwynrangel/vaccination/logger"
	"gitlab.com/edwynrangel/vaccination/service"
)

// sanityCheck validate env required
func sanityCheck() {
	envProps := []string{
		"SERVER_ADDRESS",
		"SERVER_PORT",
		"DB_USER",
		"DB_PASSWD",
		"DB_ADDR",
		"DB_PORT",
		"DB_NAME",
	}
	for _, k := range envProps {
		if os.Getenv(k) == "" {
			logger.Fatal(fmt.Sprintf("Environment variable %s not defined. Terminating application...", k))
		}
	}
}

// Start start app
func Start() {
	sanityCheck()

	dbClient := getDbClient()
	ah := AuthHandler{service: service.NewAuthService(user.NewUserRepositoryDb(dbClient))}
	uh := UserHandler{service: service.NewUserService(user.NewUserRepositoryDb(dbClient))}
	dh := DrugHandler{service: service.NewDrugService(drug.NewDrugRepositoryDb(dbClient))}
	vh := VaccinationHandler{service: service.NewVaccinationService(vaccination.NewVaccinationRepositoryDb(dbClient), drug.NewDrugRepositoryDb(dbClient))}
	router := mux.NewRouter()
	router.HandleFunc("/signup", uh.newUser).Methods(http.MethodPost)
	router.HandleFunc("/login", ah.login).Methods(http.MethodPost)
	am := AuthMiddleware{service: service.NewAuthService(user.NewUserRepositoryDb(dbClient))}
	api := router.PathPrefix("/").Subrouter()
	api.Use(am.authorizationHandler())
	api.HandleFunc("/drugs", dh.newDrug).Methods(http.MethodPost)
	api.HandleFunc("/drugs", dh.getAllDrug).Methods(http.MethodGet)
	api.HandleFunc("/drugs/{id:[0-9]+}", dh.updateDrug).Methods(http.MethodPut)
	api.HandleFunc("/drugs/{id:[0-9]+}", dh.removeDrug).Methods(http.MethodDelete)
	api.HandleFunc("/vaccination", vh.newVaccination).Methods(http.MethodPost)
	api.HandleFunc("/vaccination", vh.getAllVaccination).Methods(http.MethodGet)
	api.HandleFunc("/vaccination/{id:[0-9]+}", vh.updateVaccination).Methods(http.MethodPut)
	api.HandleFunc("/vaccination/{id:[0-9]+}", vh.removeVaccination).Methods(http.MethodDelete)

	// starting server
	address := os.Getenv("SERVER_ADDRESS")
	port := os.Getenv("SERVER_PORT")
	logger.Info(fmt.Sprintf("Starting server on %s:%s", address, port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%s", address, port), router))
}

// writeResponse write response for the endpoints
func writeResponse(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	if !reflect.ValueOf(data).IsZero() {
		if err := json.NewEncoder(w).Encode(data); err != nil {
			panic(err)
		}
	}
}

// getDbClient generates the client for the database
func getDbClient() *sqlx.DB {
	dbUser := os.Getenv("DB_USER")
	dbPasswd := os.Getenv("DB_PASSWD")
	dbAddr := os.Getenv("DB_ADDR")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_NAME")

	dataSource := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbAddr, dbPort, dbUser, dbPasswd, dbName)
	client, err := sqlx.Open("postgres", dataSource)
	if err != nil {
		panic(err)
	}

	return client
}
