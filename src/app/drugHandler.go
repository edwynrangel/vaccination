package app

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/edwynrangel/vaccination/dto"
	"gitlab.com/edwynrangel/vaccination/logger"
	"gitlab.com/edwynrangel/vaccination/service"
)

type DrugHandler struct {
	service service.DrugService
}

func (dh DrugHandler) newDrug(w http.ResponseWriter, r *http.Request) {
	var request dto.DrugRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Error("Error decode json")
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		drug, appErr := dh.service.NewDrug(request)
		if appErr != nil {
			writeResponse(w, appErr.Code, appErr.AsMessage())
		} else {
			writeResponse(w, http.StatusCreated, drug)
		}
	}
}

func (dh *DrugHandler) getAllDrug(writer http.ResponseWriter, request *http.Request) {

	drugs, err := dh.service.GetAllDrug()
	if err != nil {
		writeResponse(writer, err.Code, err.AsMessage())
	} else {
		writeResponse(writer, http.StatusOK, drugs)
	}

}

func (dh *DrugHandler) updateDrug(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	drugId := vars["id"]
	var request dto.DrugRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Error("Error decode json")
		writeResponse(w, http.StatusBadRequest, err.Error())
	} else {
		request.Id, err = strconv.ParseInt(drugId, 10, 64)
		if err != nil {
			logger.Error("Error parse int")
			writeResponse(w, http.StatusInternalServerError, err.Error())
		}
		drugs, err := dh.service.UpdateDrug(request)
		if err != nil {
			writeResponse(w, err.Code, err.AsMessage())
		} else {
			writeResponse(w, http.StatusOK, drugs)
		}
	}

}

func (dh *DrugHandler) removeDrug(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	drugId := vars["id"]

	err := dh.service.RemoveDrug(drugId)
	if err != nil {
		writeResponse(w, err.Code, err.AsMessage())
	} else {
		writeResponse(w, http.StatusNoContent, dto.DrugResponse{})
	}

}
