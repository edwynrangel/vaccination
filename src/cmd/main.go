package main

import (
	"gitlab.com/edwynrangel/vaccination/app"
	"gitlab.com/edwynrangel/vaccination/logger"
)

func main() {
	logger.Info("start the application...")
	app.Start()
}
